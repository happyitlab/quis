from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.utils import timezone
from .models import Question
from .forms import QuestionForm, AnswerForm
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from .forms import UserForm


# Create your views here.
def index(request):

    question_list = Question.objects.order_by('create_date')
    # question_list = Question.objects.all()
    # question_list = Question.objects.filter(id=2)
    # question_list = Question.objects.filter(content__contains='장고')
    # question_list = Question.objects.get(id=2)

    try:
        # 모델의 데이터를 수정
        change = Question.objects.get(id=1)
        change.subject = '수정합니다.(첫번째 질문)'
        change.save()

        # 모델의 데이터 삭제
        dataDelete = Question.objects.get(id=1)
        dataDelete.delete()
    except(Question.DoesNotExist):
        print('Question 모델에 해당 데이터가 존재하지 않습니다.')

    # 모델에 데이터 추가
    # dataInsert = Question(subject="세번째 질문입니다.", content="언제 끝나나요?", create_date=timezone.now())
    # dataInsert.save()

    context = {
        'question_list': question_list,
        'name': '홍길동',
        'age': 25,
        'info': {
            'count': 10,
            'sort': 15,
        }
    }

    # 입력 파라미터
    # get방식 파라미터 : localhost:8000/pybo/?page=1
    page = request.GET.get('page', '1') #page 파라미터가 없을 경우
    # print('page :', page)

    # 페이징 처리
    # question_list를 페이지 객체로 변환
    paginator = Paginator(question_list, 10) #페이지당 10개씩 게시물
    page_obj = paginator.get_page(page)
    context = {'question_list': page_obj}

    return render(request, 'edu/question_list.html', context)


def detail(request, question_id):
    question = Question.objects.get(id=question_id)
    context = {'question': question}

    return render(request, 'edu/question_detail.html', context)


def answer_create(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    question.answer_set.create(content=request.POST.get('content'), create_date=timezone.now())

    return redirect('edu:detail', question_id=question_id)


def question_create(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.create_date = timezone.now()
            question.save()
            return redirect('edu:index')
    else:
        form = QuestionForm()

    context = {'form': form}

    return render(request, 'edu/question_form.html', context)


def answer_create(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    if request.method == "POST":
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.create_date = timezone.now()
            answer.question = question
            answer.save()
            return redirect('edu:detail', question_id=question.id)
    else:
        form = AnswerForm()

    context = {'question': question, 'form': form}
    return render(request, 'edu/question_detail.html', context)


def signup(reqeust):
    if reqeust.method == "POST":
        form = UserForm(reqeust.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password = raw_password)
            login(reqeust, user)
            return redirect('index')
    else:
        form = UserForm()

    return render(request, 'edu/signup.html', {'form': form})








