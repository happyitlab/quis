from django.urls import path
from django.contrib.auth import views as auth_views
from . import views


#namespace (네임스페이스)
app_name = 'edu'


urlpatterns = [
    path('', views.index, name='index'),
    path('detail/<int:question_id>/', views.detail, name='detail'),
    path('answer/create/<int:question_id>/', views.answer_create, name='answer_create'),
    path('question/create/', views.question_create, name='question_create'),
    path('login/', auth_views.LoginView.as_view(template_name='edu/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', views.signup, name='signup'),
]